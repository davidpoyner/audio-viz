var artnet = require('artnet');
var http = require('http');
var express = require('express');
var app = express();
var server = http.createServer( app );
var port = process.env.PORT || 5000;
var path = require('path');

var _Sonos = require('sonos').Sonos;

//console.log(_Sonos.stop);
//var sonos = new _Sonos('172.30.215.239' );

//console.log(sonos.stop);
//artnet.connect('172.30.215.77');
//artnet.set(1, 0)

app.use('/echo', require('./lib/echo.js'));
app.use('/audio', express.static(path.join(__dirname, '/public/audio')));
app.use(express.static(path.join(__dirname, '/public')));

var WebSocketServer = require('ws').Server,
    wss= new WebSocketServer({port: 8080});

var numConnections = 0;
var audioFile = 'http://localhost:5000/audio/song2.mp3';

var getRand = function(){
  return ~~(255 * Math.random());
};

var onWebSocketServerConnection = function(ws){
  if (numConnections === 1) return;

  numConnections++;

  ws.on('close', function(){ 
    numConnections--;
    //artnet.send({ 1:0, 2:0, 3:0 });
  });
  ws.on('message', function(msg){
    var obj
    if (msg === "kick"){
      console.log('kick');
      obj = {
       1 : getRand(),
       2 : getRand(),
       3 : getRand()
      };
    //artnet.set( obj );
    }else if(msg === 'offkick'){
      obj = {
       1 : 0,
       2 : 0,
       3 : 0
      }
    }else if(msg === 'ready'){
      console.log('ready');
      ws.send('ready');
      //sonos.play(audioFile, function(){ 
        //sonos.stop(function(){});
        //console.log('playing');
      //});
      //sonos.stop(function(){});
    }else{
      var data = JSON.parse(msg);
      console.log(data);
      //sonos.seek(data.time, function(){sonos.play(function(){});});
    }
  });
}

wss.on('connection', onWebSocketServerConnection);

function onServerConnection( req, res ){
  res.render('public/index.html');
}

server.listen(port, app.get('ip'));
