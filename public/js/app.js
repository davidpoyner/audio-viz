(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
// stats.js - http://github.com/mrdoob/stats.js
var Stats=function(){var l=Date.now(),m=l,g=0,n=Infinity,o=0,h=0,p=Infinity,q=0,r=0,s=0,f=document.createElement("div");f.id="stats";f.addEventListener("mousedown",function(b){b.preventDefault();t(++s%2)},!1);f.style.cssText="width:80px;opacity:0.9;cursor:pointer";var a=document.createElement("div");a.id="fps";a.style.cssText="padding:0 0 3px 3px;text-align:left;background-color:#002";f.appendChild(a);var i=document.createElement("div");i.id="fpsText";i.style.cssText="color:#0ff;font-family:Helvetica,Arial,sans-serif;font-size:9px;font-weight:bold;line-height:15px";
i.innerHTML="FPS";a.appendChild(i);var c=document.createElement("div");c.id="fpsGraph";c.style.cssText="position:relative;width:74px;height:30px;background-color:#0ff";for(a.appendChild(c);74>c.children.length;){var j=document.createElement("span");j.style.cssText="width:1px;height:30px;float:left;background-color:#113";c.appendChild(j)}var d=document.createElement("div");d.id="ms";d.style.cssText="padding:0 0 3px 3px;text-align:left;background-color:#020;display:none";f.appendChild(d);var k=document.createElement("div");
k.id="msText";k.style.cssText="color:#0f0;font-family:Helvetica,Arial,sans-serif;font-size:9px;font-weight:bold;line-height:15px";k.innerHTML="MS";d.appendChild(k);var e=document.createElement("div");e.id="msGraph";e.style.cssText="position:relative;width:74px;height:30px;background-color:#0f0";for(d.appendChild(e);74>e.children.length;)j=document.createElement("span"),j.style.cssText="width:1px;height:30px;float:left;background-color:#131",e.appendChild(j);var t=function(b){s=b;switch(s){case 0:a.style.display=
"block";d.style.display="none";break;case 1:a.style.display="none",d.style.display="block"}};return{REVISION:12,domElement:f,setMode:t,begin:function(){l=Date.now()},end:function(){var b=Date.now();g=b-l;n=Math.min(n,g);o=Math.max(o,g);k.textContent=g+" MS ("+n+"-"+o+")";var a=Math.min(30,30-30*(g/200));e.appendChild(e.firstChild).style.height=a+"px";r++;b>m+1E3&&(h=Math.round(1E3*r/(b-m)),p=Math.min(p,h),q=Math.max(q,h),i.textContent=h+" FPS ("+p+"-"+q+")",a=Math.min(30,30-30*(h/100)),c.appendChild(c.firstChild).style.height=
a+"px",m=b,r=0);return b},update:function(){l=this.end()}}};"object"===typeof module&&(module.exports=Stats);

},{}],2:[function(require,module,exports){
Stats = require('stats-js');

var stats, audioFile, audio, fft, ctx, _dancer, _w, _h, ws, onUpdate;

stats = new Stats();
stats.domElement.style.position = "absolute";
stats.domElement.style.right = "0px";
stats.domElement.style.top = "0px";
document.body.appendChild(stats.domElement);

ws = new WebSocket('ws://localhost:8080');

audioFile = 'http://localhost:5000/audio/song4.mp3'

_dancer = new Dancer();

audio = new Audio();
audio.src = audioFile;
audio.oncanplay = function(){
  console.log('loaded');
  ws.send('ready');
  _dancer.load(audio);
};

var init = function(){
  var checkStats, kick;
  fft = document.getElementById('fft');
  ctx = fft.getContext('2d');
  time = document.getElementById('time');

  _dancer.fft( fft, {fillStyle: "#666" });

  circle = document.getElementById('circle');

  function resize(){
    _w = window.innerWidth;
    _h = window.innerHeight;

    circle.style.left = ((_w / 2) - 15) + "px";
    circle.style.top = ((_h / 2) - 15) + "px";

    fft.style.width = _w + "px";
  }

  resize();

  window.onresize = resize

  _dancer.play();
  setTimeout( 
    function(){
      var obj = { time: _dancer.getTime() };
      ws.send(JSON.stringify(obj));
    }
  , 1000);

  checkStats = function(){
    stats.begin();
    stats.end();
    window.requestAnimationFrame(checkStats);
  }

  checkStats();

  onUpdate = function(){
    time.innerHTML = (_dancer.getTime()) + 's';
  }

  kick = _dancer.createKick({
    threshold:0.2,
    frequency:[2, 16],
    //decay:0.005,
    onKick: function ( mag ) {
      circle.className = "onkick";
      ws.send('kick');
      ctx.fillStyle = "red"
    },
    offKick: function ( mag ) {
      circle.className = "offkick";
      ctx.fillStyle = "#666";
      //ws.send('offkick');
    }
  });

  kick.on();

  _dancer.bind('update', onUpdate);
}

ws.onmessage = init;


},{"stats-js":1}]},{},[2]);
