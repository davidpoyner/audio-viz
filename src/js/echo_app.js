var d3 = require('d3');
var _ = require('underscore');
var artist, song;
var result = document.getElementById('result');

var callback = function( data ){
  result.innerHTML = JSON.stringify(_.extend({}, data), null, '\t');
};

var submit = document.getElementById("submit");

submit.onclick = function(){
  event.preventDefault();
  artist = document.getElementById('artist').value;
  song = document.getElementById('song').value;

  console.dir( document.getElementById('artist'));
  console.log(artist, song);

  d3.json('/echo?artist=' + artist + '&song=' + song.replace(' ', '%20'), callback);
}
