Stats = require('stats-js');

var stats, audioFile, audio, fft, ctx, _dancer, _w, _h, ws, onUpdate;

stats = new Stats();
stats.domElement.style.position = "absolute";
stats.domElement.style.right = "0px";
stats.domElement.style.top = "0px";
document.body.appendChild(stats.domElement);

ws = new WebSocket('ws://localhost:8080');

audioFile = 'http://localhost:5000/audio/song4.mp3'

_dancer = new Dancer();

audio = new Audio();
audio.src = audioFile;
audio.oncanplay = function(){
  console.log('loaded');
  ws.send('ready');
  _dancer.load(audio);
};

var init = function(){
  var checkStats, kick;
  fft = document.getElementById('fft');
  ctx = fft.getContext('2d');
  time = document.getElementById('time');

  _dancer.fft( fft, {fillStyle: "#666" });

  circle = document.getElementById('circle');

  function resize(){
    _w = window.innerWidth;
    _h = window.innerHeight;

    circle.style.left = ((_w / 2) - 15) + "px";
    circle.style.top = ((_h / 2) - 15) + "px";

    fft.style.width = _w + "px";
  }

  resize();

  window.onresize = resize

  _dancer.play();
  setTimeout( 
    function(){
      var obj = { time: _dancer.getTime() };
      ws.send(JSON.stringify(obj));
    }
  , 1000);

  checkStats = function(){
    stats.begin();
    stats.end();
    window.requestAnimationFrame(checkStats);
  }

  checkStats();

  onUpdate = function(){
    time.innerHTML = (_dancer.getTime()) + 's';
  }

  kick = _dancer.createKick({
    threshold:0.2,
    frequency:[2, 16],
    //decay:0.005,
    onKick: function ( mag ) {
      circle.className = "onkick";
      ws.send('kick');
      ctx.fillStyle = "red"
    },
    offKick: function ( mag ) {
      circle.className = "offkick";
      ctx.fillStyle = "#666";
      //ws.send('offkick');
    }
  });

  kick.on();

  _dancer.bind('update', onUpdate);
}

ws.onmessage = init;

