module.exports = (grunt)->
  options =
    browserify:
      build:
        files:
          './public/js/app.js' : ['./src/js/app.js']
          './public/js/echo_app.js' : ['./src/js/echo_app.js']
    connect:
      all:
        options:
          port: 1338
          #hostname: '127.0.0.1'
          livereload:true
    watch:
      source:
        files: [ './src/**/*.js' ]
        tasks: [ 'browserify:build' ]
      options:
        livereload: true 



  grunt.initConfig options

  grunt.loadNpmTasks 'grunt-browserify'
  grunt.loadNpmTasks 'grunt-contrib-connect'
  grunt.loadNpmTasks 'grunt-contrib-watch'

  grunt.registerTask 'default', ['browserify:build', 'watch']
