var Echo = function( req, res ){
  //console.log(req);

  var artist = req.query.artist || 'Taylor Swift';
  var song = req.query.song || 'Shake It Off';

  var echojs = require('echojs')
  , fs = require('fs')
  , path = require('path');

  var location = path.join(__dirname, '../public/audio/song1.mp3');

  var _echo = echojs({
    key: 'SLU8WR6N1N06GTQNB'
  });

  var getSongInformation = function(id, cb){
    console.log('profiling track - ' + id);
    var options = {
      id: id,
      bucket: 'audio_summary'
    }
    _echo('track/profile').get(options, cb);
  }

  var songObject = {
    artist: artist,
    title: song
    //'':'bucket=id:7digital-US',
  }

  console.log(songObject);
  console.log('Loading file');

  var onTrackFound = function( err, json ){
    console.log('track found');
    console.log(json.response);
  };
  var onSongProfiled = function( err, json ){
    console.log('song profiled');
    console.log(json.response);  
  }

  var iter = function(song){
    //console.log(song);
  }

  var onSongFound = function( err, json ){
    //console.log(json);
    
    //json.response.songs.forEach(iter);
    res.json(json.response.songs);
    return
    //_id = json.response.songs[0].id
    
    _echo('song/profile').get({
      id: _id
    }, onSongProfiled);

    getSongInformation( _id, onTrackFound );
  }

    //_echo('song/search').get( songObject, onSongFound )
  _echo('song/search?bucket=tracks&bucket=id:7digital-US&bucket=audio_summary').get( songObject, onSongFound );
};
  
module.exports = Echo;
